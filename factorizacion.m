function [L,U] = factorizacion(a)
L = []
U = []
% descomposicion LU sin pivotacion
% a: matriz cuadrada
[n,m]=size(a);
if n~=m error('Matriz no cuadrada.'); end
for j=1:n-1
    if a(j,j)==0 error('Pivote nulo.'); end
    for i=(j+1):n
        factor=a(i,j)/a(j,j);
        a(i,j)=factor;
        for k=j+1:n
            a(i,k)=a(i,k)-factor*a(j,k);
        end
    end
end
L=tril(a,-1)+eye(n);
U=triu(a); 