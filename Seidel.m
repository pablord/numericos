function [X] = Seidel(A, B, tol,N)
X = zeros(size(A),1);
X0 = zeros(size(A),1);
k = 1;
n=size(A,1);
while(k <= N)
    for i=1:n
        temp1 = 0;
        temp2 = 0;
        for j=1:i-1
            temp1 = temp1 - (A(i,j)*X0(j));
        end
        for j=i+1:n
            temp2 = temp2 - (A(i,j)*X0(j));
        end
        X(i) = (1/A(i,i))*(temp1 + temp2 + B(i));
    end
    for i=1:n
        if(abs(X(i)-X0(i)) < tol)
            k
            return ;
        end
    end
    k=k+1;
    for i=1:n
        X0(i) = X(i);
    end
end
end