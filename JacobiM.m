function [X,RMSE,XS] = JacobiM(A, B,tol)
RMSE=[];
XS=[];
X = zeros(size(A),1);
X0 = zeros(size(A),1);
k = 1;
n=size(A,1);
error=1;
while(error>tol)
    XS = [XS,X];
    for i=1:n
        temp = 0;
        for j=1:n
            if(i ~=j)
                temp = temp-(A(i,j)*X0(j));
            end
        end
        X(i) = (1/A(i,i))*(B(i)+temp);
    end
    error = sqrt(mean((X - X0).^2));
    RMSE = [RMSE;error];
    k=k+1;
    for i=1:n
        X0(i) = X(i);
    end
end
end