function G=cholp(a)
% descomposicion de Cholesky
% a: matriz positive cuadrada simétrica
[n,m]=size(a);
if n~=m error('Matriz no cuadrada.'); end
if sum(sum(a<0,2))~=0 error('Matriz no positiva.'); end
if sum(sum(a~=a',2))~=0' error('Matriz no simetrica.'); end
G=zeros(size(a));
for k=1:n
    aux=0;
    for j=max(1,k-p):k-1
        aux=aux+G(k,j)^2;
    end
    G(k,k)=(a(k,k)-aux)^.5;
    for i=k+1:k+p
        aux=0;
        for j=max(1,k-p):k-1
            aux=aux+G(i,j)*G(k,j);
        end
    G(i,k)=(a(i,k)-aux)/G(k,k);
    end
end