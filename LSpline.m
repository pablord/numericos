function LS = LSpline(X,Y,x)
n = length(X);
for i=n-1:-1:1
    D = x - X(i);
    if(D>=0);
        break
    end;
end
D = x- X(i);
if(D<0);
    i=0;
    D = x -X(1);
end
M = (Y(i+1)-Y(i))/(X(i+1)-X(i));
LS = Y(i) + M*D;
end