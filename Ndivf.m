function F = Ndivf(x,y)
%getting the number of points from the x-vector
n = size(x,1);
if n == 1
   n = size(x,2);
end


%the 1st column in the divided differences table
for i = 1:n
   F(i,1) = y(i);
end

%the rest of the entries in the table
for i = 2:n
   for j = 2:i
      F(i,j)=(F(i,j-1)-F(i-1,j-1))/(x(i)-x(i-j+1));
   end
end